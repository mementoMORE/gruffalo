(function () {
    "use strict";

    let menuButton = document.getElementById('menu-button');
    let menuWr = document.getElementById('menu-wr');
    let menuBorder = document.getElementsByClassName('menu-border-top')[0];
    let countDownButton = document.getElementById('count-down');
    let countUpButton = document.getElementById('count-up');

    countDownButton.addEventListener("click", removeBookCount);
    countUpButton.addEventListener("click", addBookCount);

    if (menuButton) {
        menuButton.addEventListener("mousedown", menuClick);
        menuButton.addEventListener("mouseover", menuButtonHoverAdd);
        menuButton.addEventListener("mouseleave", menuButtonHoverRemove);
    } else {
        console.log('nomenubutton');
    }

    function menuClick () {
        menuButton.classList.toggle('active');
        menuWr.classList.toggle('active');
        menuBorder.style.top = "-4px";
        menuButton.parentNode.style.top = "-0px";
    }

    function menuButtonHoverAdd () {
        menuBorder.classList.add('active');
        if (!menuButton.classList.contains("active")) {
            menuBorder.style.top = "0px";
            menuButton.parentNode.style.top = "4px";
        } else {
            menuBorder.style.top = "-4px";
            menuButton.parentNode.style.top = "-4px";
        }
    }

    function menuButtonHoverRemove () {
        menuBorder.classList.remove('active');
        menuBorder.style.top = "-4px";
        menuButton.parentNode.style.top = "0px";
    }

    function removeBookCount () {
        let count = document.getElementById('book-count');
        let value = parseInt(count.value);
        if (value > 1) {
            count.value =  value - 1;
        }
    }
    function addBookCount () {
        let count = document.getElementById('book-count');
        if (parseInt(count.value) < 100) {
            count.value =  parseInt(count.value) + 1;
        }
    }
})();
